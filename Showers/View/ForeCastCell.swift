//
//  ForeCastCell.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

class ForeCastCell: UITableViewCell {

    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var imgVForecastIcon: UIImageView!
    @IBOutlet weak var lblMaxTemp: UILabel!
    @IBOutlet weak var lblMinTemp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCellWithObject(forecastObject: Forecast){
        self.lblMinTemp.text = String(format: "%@º", forecastObject.low!)
        self.lblMaxTemp.text = String(format: "%@º", forecastObject.high!)
        self.lblDay.text = AppManager.getDayOfWeek(forecastObject.date!)
        let imageUrlString = AppManager.getCompleteImageURLForCode(forecastObject.weatherCode!)
        let httpClient = HTTPNetworking(url: imageUrlString as String, method: "GET", httpBody: nil, headerFieldsAndValues: nil)
        httpClient.getResponseData({ (obj, error) -> Void in
            if (error == nil) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let imageData =  obj as! NSData?
                    let iconImage = UIImage(data: imageData!)
                    self.imgVForecastIcon.image = iconImage
                })
            }else {
                print(error, terminator: "")
            }
            
        })
    }

}
