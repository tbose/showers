//
//  ServiceManager.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

typealias ServiceCompletionHandler = (json :AnyObject?, error :NSError?) ->Void
class ServiceManager: NSObject {
    class func getServiceDataOnCompletion(serviceCompletionHandler : ServiceCompletionHandler) {
        let weatherAPI : NSString = AppManager.getCompleteWeatherURLForPlace(kRegionName)
        let weatherClient = HTTPNetworking(url: weatherAPI as String, method: "GET", httpBody: nil, headerFieldsAndValues: nil)
        weatherClient.getJsonData({ (obj , error) -> Void in
            serviceCompletionHandler(json: obj, error: error)
        })
    }
}
