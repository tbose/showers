//
//  AppManager.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

typealias WeatherResponseHandler = (todayWeather : AnyObject? , forecast :[Forecast]?, error : NSError?) -> Void
class AppManager: NSObject {

    //Get Complete url for Weather data
    class func getCompleteWeatherURLForPlace(place: String) -> String{
        let queryString = String(format: "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text='%@')&format=json", place)
        let escapedQueryString = queryString.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())
        let completeWeatherAPIString = String(format: "%@%@", kWeatherBaseAPIString, escapedQueryString!)
        return completeWeatherAPIString
    }
    //Get Complete image url for Weather status icon
    class func getCompleteImageURLForCode(code: String) -> String{
        let queryString = String(format: "%@.gif", code)
        let completeImageURLString = String(format: "%@%@", kWeatherBaseImageAPIString, queryString)
        return completeImageURLString
    }
    //Get Weather Data and populate model
    class func getWeatherDataOnCompletionHandler(weatherResponseHandler: WeatherResponseHandler){
        ServiceManager.getServiceDataOnCompletion { (json, error) -> Void in
            if let jsonDict = json as? NSDictionary{
                if let channelDict = jsonDict["query"]!["results"]!!["channel"] as AnyObject? as? NSDictionary{
                    var allForecast = [Forecast]()
                    var forcasts:NSMutableArray = []
                    forcasts = channelDict["item"]!["forecast"]!!.mutableCopy() as! NSMutableArray
                    for forecast in forcasts {
                        allForecast.append(Forecast(dict: forecast as! NSDictionary))
                    }
                    weatherResponseHandler(todayWeather: Weather(dict: channelDict), forecast: allForecast, error: nil)
                }else{
                    weatherResponseHandler(todayWeather: nil, forecast: nil, error: error)
                }
                
            }else{
                weatherResponseHandler(todayWeather: nil, forecast: nil, error: error)
            }
        }
    }
    //Date String Helper
    class func getDayOfWeek(today:String)->String? {
        let formatter  = NSDateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        if let todayDate = formatter.dateFromString(today) {
            let myCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
            let myComponents = myCalendar.components(.Weekday, fromDate: todayDate)
            let weekDay = myComponents.weekday
            switch weekDay {
            case 1:
                return "Sunday"
            case 2:
                return "Monday"
            case 3:
                return "Tuesday"
            case 4:
                return "Wednesday"
            case 5:
                return "Thursday"
            case 6:
                return "Friday"
            case 7:
                return "Saturday"
            default:
                print("Error fetching days")
                return "Day"
            }
        } else {
            return nil
        }
    }
    //Show Network Alert
    class func showNetworkAlert(){
        let alert = UIAlertController(title: "Alert", message: "Make sure your device connected to the internet", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        let navigationController = UIApplication.sharedApplication().windows[0].rootViewController as! UINavigationController
        navigationController.presentViewController(alert, animated: true, completion: nil)
    }
    //Show Loader
    class func showLoader(){
        let alert = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .Alert)
        alert.view.tintColor = UIColor.blackColor()
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(10, 5, 50, 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        loadingIndicator.startAnimating();
        
        alert.view.addSubview(loadingIndicator)
        let navigationController = UIApplication.sharedApplication().windows[0].rootViewController as! UINavigationController
        navigationController.presentViewController(alert, animated: true, completion: nil)
    }
    //Hide Loader
    class func hideLoader(){
        let navigationController = UIApplication.sharedApplication().windows[0].rootViewController as! UINavigationController
        navigationController.dismissViewControllerAnimated(false, completion: nil)
    }
}
