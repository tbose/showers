//
//  Weather.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

class Weather: NSObject {

    var humidity : String?
    var windSpeed : String?
    var weatherCondition : String?
    var temperature : String?
    var weatherCode : String?
    var date : String?
    var day : String?
    var high : String?
    var low : String?
    
    init(dict : NSDictionary) {
        self.humidity = dict["atmosphere"]!["humidity"] as AnyObject? as! String?
        self.windSpeed = dict["wind"]!["speed"] as AnyObject? as! String?
        self.weatherCondition =  dict["item"]!["condition"]!!["text"] as AnyObject? as! String?
        self.temperature = dict["item"]!["condition"]!!["temp"] as AnyObject? as! String?
        self.weatherCode =  dict["item"]!["condition"]!!["code"] as AnyObject? as! String?
        self.date = dict["item"]!["forecast"]!![0]["date"] as AnyObject? as! String?
        self.day = dict["item"]!["forecast"]!![0]["day"] as AnyObject? as! String?
        self.high = dict["item"]!["forecast"]!![0]["high"] as AnyObject? as! String?
        self.low = dict["item"]!["forecast"]!![0]["low"] as AnyObject? as! String?
    }
}
