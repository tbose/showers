//
//  Forecast.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

class Forecast: NSObject {
    var weatherCondition : String?
    var weatherCode : String?
    var date : String?
    var day : String?
    var high : String?
    var low : String?
    
    init(dict : NSDictionary) {
        self.weatherCondition =  dict["text"] as AnyObject? as! String?
        self.weatherCode =  dict["code"] as AnyObject? as! String?
        self.date = dict["date"] as AnyObject? as! String?
        self.day = dict["day"] as AnyObject? as! String?
        self.high = dict["high"] as AnyObject? as! String?
        self.low = dict["low"] as AnyObject? as! String?
    }
}
