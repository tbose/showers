//
//  HTTPNetworking.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

public typealias HTTPResponseHandler = (obj : AnyObject? , error : NSError?) -> Void

public class HTTPNetworking: NSObject {

    var httpMethod,urlString,httpBody: NSString?
    var headerFieldsAndValues : NSDictionary?
    
    //Initializer Method with url string , method, body , header field and values
    
    public init(url:String?, method:String?, httpBody: NSString?, headerFieldsAndValues: NSDictionary?) {
        self.urlString =  url
        self.httpMethod = method
        if httpBody != nil {
            self.httpBody = httpBody!
        }
        if headerFieldsAndValues != nil {
            self.headerFieldsAndValues = headerFieldsAndValues!
        }
        
    }
    //Get formatted JSON
    public func getJsonData(httpResponseHandler : HTTPResponseHandler) {
        if self.urlString != nil {
            let request = NSMutableURLRequest(URL: NSURL(string:self.urlString! as String)!)
            request.HTTPMethod =  self.httpMethod! as String
            self.headerFieldsAndValues?.enumerateKeysAndObjectsUsingBlock({ (key, value, stop) -> Void in
                request.setValue(value as! NSString as String, forHTTPHeaderField: key as! NSString as String)
            })
            request.HTTPBody = self.httpBody?.dataUsingEncoding(NSUTF8StringEncoding)
            let session = NSURLSession.sharedSession()
            let task = session.dataTaskWithRequest(request, completionHandler: { (data, response , error) -> Void in
                if (error == nil) {
                    var jsonError : NSError?
                    var json : AnyObject?
                    do {
                        json = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves)
                    } catch let error as NSError {
                        jsonError = error
                        json = nil
                    } catch {
                        fatalError()
                    }
                    if let object = json as? Array <AnyObject> {
                        httpResponseHandler(obj: object ,error: nil)
                    }else if let object = json as? Dictionary <String, AnyObject> {
                        httpResponseHandler(obj: object ,error: nil)
                    }else {
                        httpResponseHandler(obj: nil,error:jsonError)
                    }
                }else {
                    httpResponseHandler(obj: nil,error: error)
                }
            })
            task.resume()
        }else {
            httpResponseHandler(obj: nil, error: nil)
        }
    }
    
    public func getResponseData(httpResponseHandler : HTTPResponseHandler) {
        let request = NSMutableURLRequest(URL: NSURL(string:self.urlString! as String)!)
        request.HTTPMethod =  self.httpMethod! as String
        self.headerFieldsAndValues?.enumerateKeysAndObjectsUsingBlock({ (key, value, stop) -> Void in
            request.setValue(value as? String, forHTTPHeaderField: key as! NSString as String)
        })
        request.HTTPBody = self.httpBody?.dataUsingEncoding(NSUTF8StringEncoding)
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request, completionHandler: { (data, response , error) -> Void in
            if (error == nil) {
                httpResponseHandler (obj: data, error: nil)
            }else {
                httpResponseHandler(obj: nil,error: error)
            }
        })
        task.resume()
    }
}
