//
//  HomeViewController.swift
//  Showers
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    let cellIdentifier = "ForecastCell"
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblWindSpeed: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var lblPlaceName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var imgViewWeatherIcon: UIImageView!
    @IBOutlet weak var lblWeatherText: UILabel!
    @IBOutlet weak var lblHigh: UILabel!
    @IBOutlet weak var lblLow: UILabel!
    var foreCastList = [Forecast]()
    
    @IBOutlet weak var tblForecast: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppManager.showLoader()
        self.loadDatasource()
        
        // Do any additional setup after loading the view.
    }
    func loadUI(weatherObject: AnyObject){
        
        let weather = weatherObject as! Weather
        self.lblTemperature.text = String(format: "%@º", weather.temperature!)
        self.lblHumidity.text = String(format: "%@ %%", weather.humidity!)
        self.lblWindSpeed.text = String(format: "%@ mph", weather.windSpeed!)
        self.lblWeatherText.text = weather.weatherCondition
        self.lblDate.text = weather.date
        self.lblPlaceName.text = kRegionName
        self.lblHigh.text = String(format: "⇡ %@º", weather.high!)
        self.lblLow.text = String(format: "⇣ %@º", weather.low!)
        
        let imageUrlString = AppManager.getCompleteImageURLForCode(weather.weatherCode!)
        let httpClient = HTTPNetworking(url: imageUrlString as String, method: "GET", httpBody: nil, headerFieldsAndValues: nil)
        httpClient.getResponseData({ (obj, error) -> Void in
            if (error == nil) {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let imageData =  obj as! NSData?
                    let iconImage = UIImage(data: imageData!)
                    self.imgViewWeatherIcon.image = iconImage
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                })
            }else {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            
        })
        
        self.tblForecast.reloadData()
        
    }
    func loadDatasource(){
        if Reachability.isConnectedToNetwork(){
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            AppManager.getWeatherDataOnCompletionHandler { (todayWeather, forecast, error) -> Void in
                AppManager.hideLoader()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if error != nil {
                        AppManager.showNetworkAlert()
                        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    }else {
                        self.foreCastList = forecast!
                        self.loadUI(todayWeather!)
                    }
                })
                
            }
        }else{
            AppManager.hideLoader()
            AppManager.showNetworkAlert()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnRefresh(sender: AnyObject) {
        self.loadDatasource()
    }
// MARK: - TableView DataSource Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.foreCastList.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: ForeCastCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ForeCastCell
        // Configure Cell
        let forecast = self.foreCastList[indexPath.row] as Forecast
        cell.configureCellWithObject(forecast)
        return cell
    }
// MARK: - TableView Delegate Methods
    
    
}
