//
//  ShowersTests.swift
//  ShowersTests
//
//  Created by Tanumoy Bose on 20/06/16.
//  Copyright © 2016 Self. All rights reserved.
//

import XCTest
@testable import Showers

class ShowersTests: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    func testCheckForInternetConnectivity() {
        XCTAssertTrue(Reachability.isConnectedToNetwork(), "Internet Available")
    }
    func testRetrieveWeather() {
        
        let asyncExpectation = expectationWithDescription("fetchWeatherData")
        ServiceManager.getServiceDataOnCompletion { (json, error) -> Void in
            if (json != nil) {
                asyncExpectation.fulfill()
            }
        }
        self.waitForExpectationsWithTimeout(15) { error in
            XCTAssertNil(error, "Something went wrong")
            
        }
        
    }
    func testAsynchronousURLConnection() {
        let URL = NSURL(string: AppManager.getCompleteWeatherURLForPlace(kRegionName))!
        let expectation = expectationWithDescription("GET \(URL)")
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(URL) { data, response, error in
            XCTAssertNotNil(data, "data should not be nil")
            XCTAssertNil(error, "error should be nil")
            
            if let HTTPResponse = response as? NSHTTPURLResponse,responseURL = HTTPResponse.URL, MIMEType = HTTPResponse.MIMEType {
                XCTAssertEqual(responseURL.absoluteString, URL.absoluteString, "HTTP response URL should be equal to original URL")
                XCTAssertEqual(HTTPResponse.statusCode, 200, "HTTP response status code should be 200")
                XCTAssertEqual(MIMEType, "application/json", "HTTP response content type should be text/html")
            } else {
                XCTFail("Response was not NSHTTPURLResponse")
            }
            expectation.fulfill()
        }
        task.resume()
        
        waitForExpectationsWithTimeout(task.originalRequest!.timeoutInterval) { error in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            task.cancel()
        }
    }
    
}
