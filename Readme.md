# Showers - A simple weather app

This iOS app written in Swift using UIKit Framework and Yahoo weather Api


# Overview

This project is open source. Download it, use it, modify it. Find it here:
https://bitbucket.org/tbose/showers

#Topics covered

1. Using Yahoo Api in iOS App
2. Webservice Call
3. MVC architecture
4. UI with autolayout
5. Introducing App Manager which act as a domain layer


# Configuration

Before running the sample app you must change the bundle identifier in the *General* tab of your project settings to a
container identifier that you own.
Presently we set the Static region name, please change the kRegionName in the AppConstants.swift file to get the weather of your preferred location.

# Running the app

Whether you are running the app on a simulator or a device, you must be connected to Internet.
